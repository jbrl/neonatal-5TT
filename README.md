## neonatal-5TT
This is the code to create the 5TT file and the parcellation needed to create the connectome using Anatomically-constrained tractography (ACT) (Smith et al. 2012) in the neonatal brain.
Different approaches can be found in the literature (Batalle et al. 2017, Lennartsson et al. 2018, Blesa et al. 2019). The main purpose of this work was to create a fully automated method, freely available and respecting as much as possible the underlying anatomy of the neonatal brain.

## Requirements
This method relies on several different resources/tools:
- FSL (https://fsl.fmrib.ox.ac.uk/fsl/fslwiki)
- MRtrix (http://www.mrtrix.org/)
- ANTs (http://stnava.github.io/ANTs/)
- matlab (https://www.mathworks.com/products/matlab.html)
- M-CRIB atlas (https://github.com/DevelopmentalImagingMCRI/M-CRIB_atlas)
- Developing Human Connectome Project (dHCP) pipeline (https://github.com/BioMedIA/dhcp-structural-pipeline)

Note that matlab is not freely available, this small part of the code could be easily written in Octave or Python.

## Instructions
First of all you need to install all the software mentioned in the previous section and download the 10 manually labelled subjects of the M-CRIB atlas.

The next step is to run the dHCP pipeline to the M-CRIB manually labelles subjects. This is done to obtain the images bias field corrected in the same way as the subject will be later on and also the brain mask defined with the same criteria. 

This can be done with the following command: `dhcp-pipeline.sh ID session AGE -T2 T2w.nii.gz -d $PWD/Structural -t 20`

Note: to speed up things, you can modify the number of cores with the -t option

Put all the T2w of the M-CRIB processed with dHCP pipeline in a folder, with the brainmask_drawnem.nii.gz and the segmentations. Then run: 

`labelconvert ID_seg.nii.gz M-CRIB_labels_FreeSurfer_format.txt M-CRIB_labels_FreeSurfer_format_default.txt ID_seg_mrtrix.nii.gz` 

For each subject.

Note: this step has to be done only once.

The next step is to run the dHCP pipeline in all the subjects you want to process. With the following command:

`dhcp-pipeline.sh ID session AGE -T2 T2w.nii.gz -additional -d $PWD/Structural -t 20`

It is important to add the -additional option here.

**To obtain the parcellation**, first we perform intensity histogram matching between the M-CRIB templates and our subject, this can be done with the following lines of code:

```
for i in $(seq -f %02g 1 10); do
mrhistmatch -mask_input P${i}_brainmask_drawem.nii.gz -mask_target ${BASENAME}_brainmask_drawem.nii.gz scale P${i}_T2w_restore_brain.nii.gz ${BASENAME}_T2w_restore_brain.nii.gz ${BASENAME}_P${i}_hist.nii.gz
done
```
Where `P${i}_T2w_restore_brain.nii.gz` is the processed T2w of the M-CRIB atlas for the subject `${i}` and `P${i}_brainmask_drawem.nii.gz` is his mask. `${BASENAME}_T2w_restore_brain.nii.gz` in the subject of interest we want to parcellate and `P${i}_brainmask_drawem.nii.gz` it's his mask.

Then, execute the following command:

`antsJointLabelFusion.sh -d 3 -t ${BASENAME}_T2w_restore_brain.nii.gz -x ${BASENAME}_brainmask_drawem.nii.gz -o ${BASENAME}_MCRIB_Structural_ -q 0 -g ${BASENAME}_P01_hist.nii.gz -l P01_seg_mrtrix.nii.gz -g ${BASENAME}_P02_hist.nii.gz -l P02_seg_mrtrix.nii.gz -g ${BASENAME}_P03_hist.nii.gz -l P03_seg_mrtrix.nii.gz -g ${BASENAME}_P04_hist.nii.gz -l P04_seg_mrtrix.nii.gz -g ${BASENAME}_P05_hist.nii.gz -l P05_seg_mrtrix.nii.gz -g ${BASENAME}_P06_hist.nii.gz -l P06_seg_mrtrix.nii.gz -g ${BASENAME}_P07_hist.nii.gz -l P07_seg_mrtrix.nii.gz -g ${BASENAME}_P08_hist.nii.gz -l P08_seg_mrtrix.nii.gz -g ${BASENAME}_P09_hist.nii.gz -l P09_seg_mrtrix.nii.gz -g ${BASENAME}_P10_hist.nii.gz -l P10_seg_mrtrix.nii.gz`

This script takes a lot of time, because by default runs in serial, but you can add the option `-c` to control for parallel computation.

Finally, **to obtain the 5TT**, we just need to combine the obtained label from the previous step with the probability maps obtained from the dHCP pipeline.

For the GM:

`fslmaths ${BASENAME}_drawem_seg1.nii.gz -add ${BASENAME}_drawem_seg2.nii.gz -add ${BASENAME}_drawem_seg3.nii.gz -add ${BASENAME}_drawem_seg4.nii.gz -add ${BASENAME}_drawem_seg5.nii.gz -add ${BASENAME}_drawem_seg6.nii.gz -add ${BASENAME}_drawem_seg7.nii.gz -add ${BASENAME}_drawem_seg8.nii.gz -add ${BASENAME}_drawem_seg9.nii.gz -add ${BASENAME}_drawem_seg10.nii.gz -add ${BASENAME}_drawem_seg11.nii.gz -add ${BASENAME}_drawem_seg12.nii.gz -add ${BASENAME}_drawem_seg13.nii.gz -add ${BASENAME}_drawem_seg14.nii.gz -add ${BASENAME}_drawem_seg15.nii.gz -add ${BASENAME}_drawem_seg16.nii.gz -add ${BASENAME}_drawem_seg20.nii.gz -add ${BASENAME}_drawem_seg21.nii.gz -add ${BASENAME}_drawem_seg22.nii.gz -add ${BASENAME}_drawem_seg23.nii.gz -add ${BASENAME}_drawem_seg24.nii.gz -add ${BASENAME}_drawem_seg25.nii.gz -add ${BASENAME}_drawem_seg26.nii.gz -add ${BASENAME}_drawem_seg27.nii.gz -add ${BASENAME}_drawem_seg28.nii.gz -add ${BASENAME}_drawem_seg29.nii.gz -add ${BASENAME}_drawem_seg30.nii.gz -add ${BASENAME}_drawem_seg31.nii.gz -add ${BASENAME}_drawem_seg32.nii.gz -add ${BASENAME}_drawem_seg33.nii.gz -add ${BASENAME}_drawem_seg34.nii.gz -add ${BASENAME}_drawem_seg35.nii.gz -add ${BASENAME}_drawem_seg36.nii.gz -add ${BASENAME}_drawem_seg37.nii.gz -add ${BASENAME}_drawem_seg38.nii.gz -add ${BASENAME}_drawem_seg39.nii.gz ${BASENAME}-Pmap-GM.nii.gz`

For the CSF:

`fslmaths ${BASENAME}_drawem_seg49.nii.gz -add ${BASENAME}_drawem_seg50.nii.gz -add ${BASENAME}_drawem_seg83.nii.gz ${BASENAME}-Pmap-CSF.nii.gz`

For the WM:

`fslmaths ${BASENAME}_drawem_seg19.nii.gz -add ${BASENAME}_drawem_seg51.nii.gz -add ${BASENAME}_drawem_seg52.nii.gz -add ${BASENAME}_drawem_seg53.nii.gz -add ${BASENAME}_drawem_seg54.nii.gz -add ${BASENAME}_drawem_seg55.nii.gz -add ${BASENAME}_drawem_seg56.nii.gz -add ${BASENAME}_drawem_seg57.nii.gz -add ${BASENAME}_drawem_seg58.nii.gz -add ${BASENAME}_drawem_seg59.nii.gz -add ${BASENAME}_drawem_seg60.nii.gz -add ${BASENAME}_drawem_seg61.nii.gz -add ${BASENAME}_drawem_seg62.nii.gz -add ${BASENAME}_drawem_seg63.nii.gz -add ${BASENAME}_drawem_seg64.nii.gz -add ${BASENAME}_drawem_seg65.nii.gz -add ${BASENAME}_drawem_seg66.nii.gz -add ${BASENAME}_drawem_seg67.nii.gz -add ${BASENAME}_drawem_seg68.nii.gz -add ${BASENAME}_drawem_seg69.nii.gz -add ${BASENAME}_drawem_seg70.nii.gz -add ${BASENAME}_drawem_seg71.nii.gz -add ${BASENAME}_drawem_seg72.nii.gz -add ${BASENAME}_drawem_seg73.nii.gz -add ${BASENAME}_drawem_seg74.nii.gz -add ${BASENAME}_drawem_seg75.nii.gz -add ${BASENAME}_drawem_seg76.nii.gz -add ${BASENAME}_drawem_seg77.nii.gz -add ${BASENAME}_drawem_seg78.nii.gz -add ${BASENAME}_drawem_seg79.nii.gz -add ${BASENAME}_drawem_seg80.nii.gz -add ${BASENAME}_drawem_seg81.nii.gz -add ${BASENAME}_drawem_seg82.nii.gz -add ${BASENAME}_drawem_seg85.nii.gz -add ${BASENAME}_drawem_seg48.nii.gz -add ${BASENAME}_drawem_seg40 -add ${BASENAME}_drawem_seg41.nii.gz -add ${BASENAME}_drawem_seg42.nii.gz -add ${BASENAME}_drawem_seg43.nii.gz -add ${BASENAME}_drawem_seg44.nii.gz -add ${BASENAME}_drawem_seg45.nii.gz -add ${BASENAME}_drawem_seg46.nii.gz -add ${BASENAME}_drawem_seg47.nii.gz -add ${BASENAME}_drawem_seg86.nii.gz -add ${BASENAME}_drawem_seg87.nii.gz ${BASENAME}-Pmap-WM.nii.gz`

For the subcortical GM:

`fslmaths ${BASENAME}_drawem_seg17.nii.gz -add ${BASENAME}_drawem_seg18.nii.gz ${BASENAME}-Pmap-subGM.nii.gz`

The cerebellum is included as subcortical GM, by doing this, we allow the tracts to enter inside the cerebellum and not to stop in the interface between the WM and the parcellation of the cerebellum.

The WM and the subcortical GM probability maps still need to be completed. The next step is to extract the subcortical structures from the M-CRIB parcellation and combine them:

```
fslmaths ${BASENAME}_MCRIB_Structural_Labels.nii.gz -thr 36 -uthr 39 -bin ${BASENAME}_sub1.nii.gz
fslmaths ${BASENAME}_MCRIB_Structural_Labels.nii.gz -thr 43 -uthr 46 -bin ${BASENAME}_sub2.nii.gz
fslmaths ${BASENAME}_MCRIB_Structural_Labels.nii.gz -thr 42 -uthr 42 -bin ${BASENAME}_sub3.nii.gz
fslmaths ${BASENAME}_MCRIB_Structural_Labels.nii.gz -thr 49 -uthr 49 -bin ${BASENAME}_sub4.nii.gz
fslmaths ${BASENAME}_sub1.nii.gz -add ${BASENAME}_sub2.nii.gz -add ${BASENAME}_sub3.nii.gz -add ${BASENAME}_sub4.nii.gz ${BASENAME}_sub_all.nii.gz
```

Next, we invert this mask and apply it to the WM tissue probability map:

```
mrthreshold -abs 0.5 -invert ${BASENAME}_sub_all.nii.gz ${BASENAME}_sub_all_inverted.nii.gz
fslmaths ${BASENAME}-Pmap-WM.nii.gz -mul ${BASENAME}_sub_all_inverted.nii.gz ${BASENAME}-Pmap-WM-corr.nii.gz
```

We combine the subcortical GM tissue probability map with the structures derived from the M-CRIB parcellation and normalize all the maps between 0 and 1.

```
fslmaths ${BASENAME}-Pmap-GM.nii.gz -div 100 ${BASENAME}-Pmap-0001.nii.gz
fslmaths ${BASENAME}-Pmap-subGM.nii.gz -div 100 ${BASENAME}-Pmap-0002_pre.nii.gz
fslmaths ${BASENAME}-Pmap-0002_pre.nii.gz -add ${BASENAME}_sub_all.nii.gz ${BASENAME}-Pmap-0002.nii.gz
fslmaths ${BASENAME}-Pmap-WM-corr.nii.gz -div 100 ${BASENAME}-Pmap-0003.nii.gz
fslmaths ${BASENAME}-Pmap-CSF.nii.gz -div 100 ${BASENAME}-Pmap-0004.nii.gz
```

To ensure that the sum of all the tissue probability maps in all the voxels is equal to 1, we run the following command:

``matlab -nodesktop -nosplash -r "clc; clear all; procRoot='/niftilibraries'; addpath([procRoot '/UTILITIES/NIfTI_20140122/']); STRUCTURAL=load_untouch_nii('${BASENAME}_T2w_restore_brain.nii.gz'); x=STRUCTURAL.hdr.dime.dim(2); y=STRUCTURAL.hdr.dime.dim(3); z=STRUCTURAL.hdr.dime.dim(4); bigMat=zeros(x*y*z,4); for i=1:4; nii=load_untouch_nii(strcat('${BASENAME}-Pmap-000',num2str(i),'.nii.gz')); bigMat(:,i)=nii.img(:); end; bigMat2=bigMat; for j=1:x*y*z; bigMat2(j,:)=bigMat(j,:)./sum(bigMat(j,:)); end; for k=1:4; nii=load_untouch_nii(strcat('${BASENAME}-Pmap-000',num2str(i),'.nii.gz')); nii.img=reshape(bigMat2(:,k),[x y z]); save_untouch_nii(nii,strcat('${BASENAME}-normalized-Pmap-000',num2str(k))); end; exit;"``

Replacing `procRoot='/niftilibraries'; addpath([procRoot '/UTILITIES/NIfTI_20140122/'])` with the path to your nifti library.

Finally, you just need to merge the files (adding a blanc tissue type at the end for the case of healthy brains) and remove all the NaN values.

```
fslmaths ${BASENAME}_T2w_restore_brain.nii.gz -mul 0 ${BASENAME}-normalized-Pmap-0005.nii.gz
fslmerge -t ${BASENAME}-5TTnan.nii.gz ${BASENAME}-normalized-Pmap-0001.nii ${BASENAME}-normalized-Pmap-0002.nii ${BASENAME}-normalized-Pmap-0003.nii ${BASENAME}-normalized-Pmap-0004.nii ${BASENAME}-normalized-Pmap-0005.nii.gz
fslmaths ${BASENAME}-5TTnan.nii.gz -nan ${BASENAME}-5TT.nii.gz
```
The result for this pipeline can be seen in the following figure:

![Alt text](5TT.png?raw=true "Title")

The first row is a preterm infant from the [Theirworld Edinburgh Birth Cohort](http://www.tebc.ed.ac.uk/) and the second is a term born baby (subject CC00069XX12) from the [dHCP](http://www.developingconnectome.org/).

## How to cite
If you are using this method, please cite:

**Hierarchical complexity of the macro-scale neonatal brain**. Manuel Blesa, Paola Galdi, Simon R. Cox, Gemma Sullivan, David Q. Stoye, Gillian J. Lamb, Alan J. Quigley, Michael J. Thrippleton, Javier Escudero, Mark E. Bastin, Keith M. Smith and James P. Boardman.
_Cerebral Cortex_, Volume 31, Issue 4, April 2021, Pages 2071–2084, https://doi.org/10.1093/cercor/bhaa345 

Also be aware that all the tools employed here have their own citations, please cite them appropriately.

## Contact:

For any doubt/suggestion please email me to: manuel.blesa@ed.ac.uk


